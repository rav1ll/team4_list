class Node:
    def __init__(self, val, next_element: 'Node' = None):
        self.val = val
        self.next_element = next_element


class LinkedList():
    def __init__(self, head: Node = None):
        self.head = head

    def __str__(self) -> str:
        result = ''
        print_val = self.head
        while print_val:
            result += str(print_val.val) + ' '
            print_val = print_val.next_element
        return result

    def __len__(self):
        current_element = self.head
        current_count = 0
        while current_element:
            current_count += 1
            current_element = current_element.next_element
        return current_count

    def __getitem__(self, idx):
        current_element = self.head
        index = 0
        while index <= idx:
            if index == idx:
                return current_element.val
            index += 1
            current_element = current_element.next_element

    def push(self, x):
        new_head = Node(x, self.head)
        self.head = new_head

    def elem_delete(self, index):

        if index == 0:
            temp = self.head
            self.head = None
            if temp.next_element is not None:
                self.head = temp.next_element

        else:
            for i in range(index - 1):
                self.head = self.head.next_element

            tmp = self.head

            tmp_nxt = tmp.next_element

            if tmp_nxt.next_element is not None:
                tmp.next_element = tmp_nxt.next_element

    def insert(self, element, index):
        i = 0
        node = self.head
        prev_node = self.head

        while i < index:
            prev_node = node
            node = node.next_element
            i += 1

        prev_node.next_element = Node(element, next_element=node)
        return element

    def check_empty(self):
        if self.head is None:
            print('empty list')
        else:
            print('not empty list')


class Iter():
    def __init__(self, element):
        self.val = element.val
        self.next_element = element.next_element

    def __iter__(self):
        return self

    def __next__(self):
        next_elt = self.next_element

        if self.val is None or self.next_element is None:
            raise StopIteration()

        else:
            next_elt = self.next_element
            return next_elt.val


if __name__ == '__main__':
    lst = LinkedList()
    lst.head = Node(3)
    lst.head.next_element = Node(5)
    lst.push(1)
    print(lst.head.val)
    print(lst[1])
    print(len(lst))
    print(lst)
    lst.elem_delete(0)
    lst.insert(666, 1)
    print(lst)

    print(Iter.__next__(lst.head))

    for i in range(len(lst)):
        print(Iter.__next__(lst.head))

        lst.head = lst.head.next_element
